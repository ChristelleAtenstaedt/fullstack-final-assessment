

# TASK 1 


# // TASK 1 

def isPalindrome(word): 
  if word ==  word[::-1]:
    return('true')
  else:
    return('false')

# Big 0 and time/space complexity - would be O(N) as it is dependent on the length of the string (word), so it is linear. The space complexity would be O(1) as no extra space would be needed.


print(isPalindrome("abcdcba"))  # TRUE 
print(isPalindrome("aba")) # TRUE 
print(isPalindrome("c"))  # TRUE
print(isPalindrome("radar"))  # TRUE
print(isPalindrome("level"))  # TRUE 
print(isPalindrome("pencil"))  # # TRUE
print(isPalindrome("ark"))  # TRUE
print(isPalindrome("aa"))  # TRUE



#  TASK 2

isMissing (arrayInput):
    missing = 0
    return `${missing} is missing`



print(isMissing([1, 2, 3, 4, 5]))  # nothing is missing 
print(isMissing([4,5,1,3, 5, 6])) #  2 is missing 
print(isMissing([ 1, 3, 7, 5, 6, 2 ])) #  4 is missing
print(isMissing([4,5,-1,3, 5]))  # THROW ERROR Invalid input, non-numeric value detected 
print(isMissing([ 1, 3, 7, 5, 6, 2 ])) #  THROW ERROR Invalid input, non-numeric value detected
 

#  //what i would do firstly is to sort the array, the difference between each elemtn and the one next to it should be 1, aso then I would iterate over the list and find out which elemnts have more of a gap that +1
# I have run out of time but I believe that this aproach would have a time complecity of O(N) as it dependent on the size of the array and this linear, similarly it would have space complexity of O(N)