// CFG FULLSTACK FINAL ASSESSMENT - CHRISTELLE ATENSTAEDT

// PART 1
// 1.Can you provide a brief summary of what is happening in this function code?

// In this function code, firstly this fucntional component (countReducer) has had { useState } hook imported into the file, this enables it to become a stateful component and therefore enables us to be able to track and manage this student counter.  This function is a reducer, a pure function that takes in two arguments, the current state (initialState) and an action. The action type here is 'increment', this will be dispatched when triggered by the component, notifying the reducer in what way the state neeeds to be altered. In terms of this appplication, this menas that each time this action is performed, the student count will increment by 1 and a brand new state is returned which is the inital state, with 1 added to it. 

// 2.Add one action that tells the reducer to reduce the state value by 1

import React from 'react'

function countReducer(state = initialState, action) {
    if (action.type === 'increment') {
    return (
        value: state.value + 1
    };
    else if (action.type === 'decrement') {
    return (
        value: state.value - 1
    };
return state
}

// I feel it should be written more like this however:
// import React, { useState } from 'react'

// const countReducer = () => {
//     const [initialState, setCount] = useState(0);
//     const increment = () => {
//         setCount(initialState + 1);
//     };

//     const decrement = () => {
//             setCount(initialState - 1);
//     };
    
    

// 3.Add one action that tells the reducer to reset the state


import React from 'react'

function countReducer(state = initialState, action) {
    if (action.type === 'increment') {
      return {
        value: state.value + 1
    };
    else if (action.type === 'decrement') {
      return {
        value: state.value - 1
    };
    default (action.type === 'reset') {
        return {
            value: state.value === 0
        };
    }
return state
}

// PART 2
// 1.
// liNE 34: Here, our functional component, classInfo is taking in two arguments, studentCount which is the initial state and setStudentCount which is the action that need sto be taken whrn this reducer is triggered. The count is initlaly set to 0. 
// Line 39: The HTML button is set to receive the data via props, here onClick - which is an evemt that allows a JS function to be executed each time it is triggered (when the button is clicked) the increment action should be dipatched as the student count needs to increment by 1 each time the 'Add Student' button is clicked on the application, and the overall count updated and a brand new one returned with one added to the studentsCount.

// 2. 
// a. I would write a function classInfo that that loops through this dictionary object, using the map() method and also use an if/else statement to add only those students whose values are set to 'True'. The useState hook would be implemented and state would be set to True initially,and the counter to 0 initially. If the value is True, then the setStudentCounter will increment by 1, if false then the state will not change/update.
// b. I would ensure that the function is triggered when the button is clicked by including the onClick event (attribute property)  Once the HTML button is clicked, this JavaScript function is thus triggered/called and is executed. The function classInfo will include a document.getElementById which will render it in the appropriate div within the page.
//c. The state will be updated vis the usestate hook, set to O initially, once the dict object has been looped through then the state will have been updated each time a value is True, then a new state will be returned with the updated, brand new student count.

// 3.
// a. useDispatch is a hook to use as alternatives to the existing connect() higher-order component, it updates the value - in Redux
// b. would use, the useDispatch hook - store
// c. 

